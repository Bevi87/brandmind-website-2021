import Head from "next/head";
import styled from "styled-components";
import { useState } from "react";

const Container = styled.div`
  min-height: 160vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-image: url("/backgrounds/contact.svg");

  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;
  h2 {
    width: 70vw;
    margin-top: 25px;
    text-align: center;

    @media (max-width: 600px) {
      margin-top: 35px;
    }
  }

  @media (max-width:600px)
  {
    min-height:180vh;
  }
`;

const Content = styled.form`
  display: grid;
  width: 85vw;
  height: 70vh;
  margin-top: 50px;
  text-align: center;
  color: white;
  grid-gap: 1rem;
  grid-template-columns: 4fr;
  grid-template-rows: 0.5fr 0.5fr 0.5fr 0.5fr 1.5fr 0.5fr;
  grid-template-areas:
    "div1" "div2"
    "div3" "div4"
    "div5" "div6";
`;

const Name = styled.div`
  grid-area: div1;

  label {
    padding-right: 15px;
  }
  input {
    background-color: transparent;
    color: white;
    border-color: white;
    border-radius: 15px;
    width: 400px;
    height: 45px;
    padding-left: 20px;
    font-family: Open-sans;
    font-size: 24px;
    font-weight: bold;
    ::placeholder {
      color: yellow;
      opacity: 0.5;
    }
    &:focus {
      outline: none;
      background-color: transparent;
    }

    @media (max-width: 600px) {
      width: 200px;
      font-size: 16px;
    }


    
    @media (max-width: 281px) {
      width: 160px;
      font-size: 16px;
    }
  }
`;
const Email = styled.div`
  grid-area: div2;
  label {
    padding-right: 15px;
  }
  input {
    background-color: transparent;
    color: white;
    border-color: white;
    border-radius: 15px;
    width: 400px;
    height: 45px;
    padding-left: 20px;
    font-family:Open-sans;
    font-size: 24px;
    font-weight: bold;
    ::placeholder {
      color: yellow;
      opacity: 0.5;
    }
    &:focus {
      outline: none;
      background-color:transparent;
    }

    @media (max-width: 600px) {
      width: 200px;
      font-size: 16px;
    }

    @media (max-width: 281px) {
      width: 160px;
      font-size: 16px;
    }
`;

const Phone = styled.div`
  grid-area: div3;
  label {
    padding-right: 15px;
  }
  input {
    background-color: transparent;
    color: white;
    border-color: white;
    border-radius: 15px;
    width: 400px;
    height: 45px;
    padding-left: 20px;
    font-family:Open-sans;
    font-size: 24px;
    font-weight: bold;
    ::placeholder {
      color: yellow;
      opacity: 0.5;
    }
    &:focus {
      outline: none;
      background-color:transparent;
    }

    @media (max-width: 600px) {
      width: 200px;
      font-size: 16px;
    }

    @media (max-width: 281px) {
      width: 160px;
      font-size: 16px;
    }
`;
const Company = styled.div`
  grid-area: div4;
  label {
    padding-right: 15px;
  }
  input {
    background-color: transparent;
    color: white;
    border-color: white;
    border-radius: 15px;
    width: 400px;
    height: 45px;
    padding-left: 20px;
    font-family:Open-sans;
    font-size: 24px;
    font-weight: bold;
    ::placeholder {
      color: yellow;
      opacity: 0.5;
    }
    &:focus {
      outline: none;
      background-color:transparent;
    }

    @media (max-width: 600px) {
      width: 200px;
      font-size: 16px;
    }

    @media (max-width: 281px) {
      width: 160px;
      font-size: 16px;
    }
`;
const Message = styled.div`
  grid-area: div5;
  label {
    padding-right: 15px;
    align-items: center;
  }
  textarea {
    vertical-align: top;
    background-color: transparent;
    color: white;
    border-color: white;
    width: 400px;
    height: 145px;
    border-radius: 15px;
    padding-left: 20px;
    font-family: Open-sans;
    font-size: 24px;
    font-weight: bold;
    ::placeholder {
      color: yellow;
      opacity: 0.5;
    }
    &:focus {
      outline: none;
      background-color: transparent;
    }
    @media (max-width: 600px) {
      width: 200px;
      font-size: 16px;
      text-align: center;
    }

    @media (max-width: 281px) {
      width: 160px;
      font-size: 16px;
    }
  }
`;

const Send = styled.div`
  grid-area: div6;

  input {
    background-color: transparent;
    border-color: white;
    color: white;
    width: 125px;
    height: 45px;
    border-radius: 15px;
    font-size: 20px;
    cursor: pointer;

    @media(max-width:600px){
      width: 75px;
      font-size: 14px;
    }
  }
`;

const contactus = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [company, setCompany] = useState("");
  const [message, setMessage] = useState("");
  const [submit, setSubmit] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Sending");

    let data = {
      name,
      email,
      phone,
      company,
      message,
    };
    fetch("/api/contact", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }).then((res) => {
      console.log("Response received");
      if (res.status === 200) {
        console.log("Response succeeded!");
        alert("Your message has been sent! Thank you for contacting us!");
        setSubmit(true);
        setName(" ");
        setEmail(" ");
        setMessage(" ");
        setPhone(" ");
        setCompany(" ");
      }
    });

    document.getElementById("formID").reset();
  };
  return (
    <Container>
      <Head>
        <title>Brandmind - ContactUs</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <h2>
        Contact us and we will provide you more information about <br />
        our producs and services.
      </h2>
      <Content id="formID">
        <Name>
          <label htmlFor="name">
            <svg
              width="24"
              height="24"
              viewBox="0 0 16 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M5.6 7.8C5.048 7.8 4.6 8.248 4.6 8.8C4.6 9.352 5.048 9.8 5.6 9.8C6.152 9.8 6.6 9.352 6.6 8.8C6.6 8.248 6.152 7.8 5.6 7.8ZM10.4 7.8C9.848 7.8 9.4 8.248 9.4 8.8C9.4 9.352 9.848 9.8 10.4 9.8C10.952 9.8 11.4 9.352 11.4 8.8C11.4 8.248 10.952 7.8 10.4 7.8ZM8 0C3.584 0 0 3.584 0 8C0 12.416 3.584 16 8 16C12.416 16 16 12.416 16 8C16 3.584 12.416 0 8 0ZM8 14.4C4.472 14.4 1.6 11.528 1.6 8C1.6 7.768 1.616 7.536 1.64 7.312C3.528 6.472 5.024 4.928 5.808 3.016C7.256 5.064 9.64 6.4 12.336 6.4C12.96 6.4 13.56 6.328 14.136 6.192C14.304 6.76 14.4 7.368 14.4 8C14.4 11.528 11.528 14.4 8 14.4Z"
                fill="white"
              />
            </svg>
          </label>
          <input
            type="text"
            name="name"
            placeholder="Firstname Surname *"
            required
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </Name>

        <Email>
          <label htmlFor="email">
            <svg
              width="22"
              height="18"
              viewBox="0 0 16 13"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.4 0H1.6C0.72 0 0.00799999 0.72 0.00799999 1.6L0 11.2C0 12.08 0.72 12.8 1.6 12.8H14.4C15.28 12.8 16 12.08 16 11.2V1.6C16 0.72 15.28 0 14.4 0ZM14.4 3.2L8 7.2L1.6 3.2V1.6L8 5.6L14.4 1.6V3.2Z"
                fill="white"
              />
            </svg>
          </label>
          <input
            type="email"
            name="email"
            placeholder="example@gmail.com *"
            required
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </Email>
        <Phone>
          <label htmlFor="phone">
            <svg
              width="22"
              height="22"
              viewBox="0 0 16 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M3.21778 6.92444C4.49778 9.44 6.56 11.4933 9.07556 12.7822L11.0311 10.8267C11.2711 10.5867 11.6267 10.5067 11.9378 10.6133C12.9333 10.9422 14.0089 11.12 15.1111 11.12C15.6 11.12 16 11.52 16 12.0089V15.1111C16 15.6 15.6 16 15.1111 16C6.76444 16 0 9.23556 0 0.888889C0 0.4 0.4 0 0.888889 0H4C4.48889 0 4.88889 0.4 4.88889 0.888889C4.88889 2 5.06667 3.06667 5.39556 4.06222C5.49333 4.37333 5.42222 4.72 5.17333 4.96889L3.21778 6.92444Z"
                fill="white"
              />
            </svg>
          </label>
          <input
            type="tel"
            name="phone"
            placeholder="+41 00 000 00 00"
            pattern="+[0-9]{2}[0-9]{10}"
            title="+CountryCodePhoneNumber"
            maxLength="13"
            onChange={(e) => {
              setPhone(e.target.value);
            }}
          />
        </Phone>
        <Company>
          <label htmlFor="company">
            <svg
              width="22"
              height="22"
              viewBox="0 0 16 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.4 3.2H11.2V1.6C11.2 0.712 10.488 0 9.6 0H6.4C5.512 0 4.8 0.712 4.8 1.6V3.2H1.6C0.712 3.2 0.00799999 3.912 0.00799999 4.8L0 13.6C0 14.488 0.712 15.2 1.6 15.2H14.4C15.288 15.2 16 14.488 16 13.6V4.8C16 3.912 15.288 3.2 14.4 3.2ZM9.6 3.2H6.4V1.6H9.6V3.2Z"
                fill="white"
              />
            </svg>
          </label>
          <input
            type="text"
            name="message"
            placeholder="Company Name"
            onChange={(e) => {
              setCompany(e.target.value);
            }}
          />
        </Company>
        <Message>
          <label htmlFor="message">
            <svg
              width="22"
              height="22"
              viewBox="0 0 16 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M15.3772 4.90001C14.9757 3.94381 14.3916 3.07469 13.6575 2.34109C12.9256 1.60621 12.0561 1.02194 11.0986 0.621444C10.1176 0.20918 9.06358 -0.00214705 7.99916 1.64452e-05H7.96337C6.88073 0.00537358 5.83388 0.219659 4.84788 0.641087C3.8985 1.04568 3.03722 1.63099 2.31218 2.3643C1.58516 3.09625 1.00777 3.96234 0.612163 4.9143C0.201503 5.90423 -0.0065805 6.96613 0.000158605 8.03751C0.00552706 9.27679 0.302582 10.5071 0.857323 11.6071V14.3214C0.857323 14.775 1.22596 15.1429 1.6787 15.1429H4.39514C5.5026 15.7005 6.72471 15.9939 7.96516 16H8.00274C9.07285 16 10.109 15.7929 11.086 15.3875C12.0388 14.9918 12.905 14.4143 13.636 13.6875C14.3733 12.9571 14.9531 12.1036 15.3593 11.1518C15.7798 10.1661 15.9946 9.11786 16 8.03572C16.0035 6.94822 15.7924 5.89287 15.3772 4.90001ZM4.42735 8.85715C3.95492 8.85715 3.57018 8.47322 3.57018 8.00001C3.57018 7.52679 3.95492 7.14287 4.42735 7.14287C4.89977 7.14287 5.28451 7.52679 5.28451 8.00001C5.28451 8.47322 4.90156 8.85715 4.42735 8.85715ZM7.99916 8.85715C7.52674 8.85715 7.142 8.47322 7.142 8.00001C7.142 7.52679 7.52674 7.14287 7.99916 7.14287C8.47159 7.14287 8.85633 7.52679 8.85633 8.00001C8.85633 8.47322 8.47159 8.85715 7.99916 8.85715ZM11.571 8.85715C11.0986 8.85715 10.7138 8.47322 10.7138 8.00001C10.7138 7.52679 11.0986 7.14287 11.571 7.14287C12.0434 7.14287 12.4281 7.52679 12.4281 8.00001C12.4281 8.47322 12.0434 8.85715 11.571 8.85715Z"
                fill="white"
              />
            </svg>
          </label>
          <textarea
            type="text"
            name="message"
            placeholder="Write Your Message here *"
            required
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />
        </Message>

        <Send>
          <input
            type="submit"
            onClick={(e) => {
              handleSubmit(e);
            }}
          />
        </Send>
      </Content>
    </Container>
  );
};

export default contactus;
