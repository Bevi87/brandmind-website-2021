import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  min-height: 180vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url("/backgrounds/datascience.svg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;

  @media (max-width: 1240px) {
    min-height: 275vh;
  }

  @media (max-width: 400px) {
    min-height: 300vh;
  }
`;

const Content = styled.div`
  margin-top: 30vh;
  display: grid;
  height: 100vh;
  width: 85vw;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 0.3fr 0.3fr 3.4fr;
  grid-template-rows: 0.5fr 3.5fr;
  grid-template-areas:
    "div1 div2 div3"
    "div1 div2 div4";

  @media (max-width: 769px) {
    grid-row-gap: 2rem;
    grid-column-gap: 0.5rem;
    width: 95vw;
    grid-template-column: 0.3fr 0.3fr 3.4fr;
    grid-template-row: 0.5fr 3.5fr;
    grid-template-areas:
      "div1 div2 div3"
      "div4 div4 div4";
  }
`;

const Icon1 = styled.div`
  grid-area: div1;

  img {
    @media (max-width: 540px) {
      width: 50px;
      height: 50px;
    }
  }
`;

const Icon2 = styled.div`
  grid-area: div2;
  img{
    @media (max-width: 540px) {
      width:50px;
      height:50px;
    }
`;

const Heading = styled.div`
  grid-area: div3;
  color: rgb(226, 220, 220);
  padding-top: 10px;
  font-weight: bold;
  font-size: 40px;
  font-family: open-sans;
  text-align: left;

  @media (max-width: 870px) {
    font-size: 35px;
  }

  @media (max-width: 540px) {
    font-size: 25px;
    padding-top: 1px;
  }
`;

const Description = styled.div`
  grid-area: div4;
  font-size: 25px;
  color: white;
  font-family: open-sans;
  text-align: left;
  line-height: 40px;

  @media (max-width: 870px) {
    font-size: 22px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
    padding-top: 1px;
    line-height: 30px;
  }
`;

const dataScience = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Consulting</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Content>
        <Icon1>
          <Link href="/consultingbrain" passHref>
            <img
              src="/icons/backicon.svg"
              alt="Back"
              style={{ cursor: "pointer" }}
            />
          </Link>
        </Icon1>
        <Icon2>
          <img src="/icons/datascienceicon.svg" alt="Icon" />
        </Icon2>
        <Heading>Data Science - 360 Customer View</Heading>
        <Description>
          <p>
            Existing persona segmentations are often not significant enough for
            accurate operational marketing activities and past oriented data
            only give insight about the past.
          </p>
          <br />
          <p>
            Many companies lack in their data of the intelligence and the
            in-depth to create a robust foundation of a segmentation that could
            make their marketing activities much more efficient. Only the
            extension of existing data with psychometric profiles and their
            sensory preferences form the basis for an individual customer
            approach.
          </p>
          <br />
          <p>
            The collected anonymous psychometric profiles will be matched with
            existing anonymous customer data and searched for correlations with
            the socio-demographic, geographical data as well as with data on the
            type of purchase / purchase frequency. Based on our predictive
            algorithms, an extrapolation to the entire customer base is then
            carried out.
          </p>
          <br />
          <p>
            For companies who still do not have enough existing customer data,
            we can support them by using the playful psychological test at all
            own digital touchpoints to collect additional data.
          </p>
          <br />
          <p>
            In this way we help companies to{" "}
            <b>
              <i>develop a consistent</i>
            </b>{" "}
            and{" "}
            <b>
              <i>in-depth segmentation</i>
            </b>{" "}
            and to{" "}
            <b>
              <i>
                implement the efficient and profound intelligence for marketing
                activities.
              </i>
            </b>
          </p>
        </Description>
      </Content>
    </Container>
  );
};

export default dataScience;
