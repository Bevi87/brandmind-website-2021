import Head from "next/head";
import styled from "styled-components";

import Link from "next/link";

const Container = styled.div`
  min-height: 160vh;
  display: flex;
  flex-direction: column;
  text-align: center;
  align-items: center;
  background-image: url("/backgrounds/teambg.svg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;

  @media (max-width: 992px) {
    min-height: 220vh;
  }

  @media (max-width: 767px) {
    min-height: 250vh;
  }

  @media (max-width: 500px) {
    min-height: 200vh;
  }
`;
const Team = styled.div`
  margin-top: 200px;
  display: grid;
  height: 100vh;
  width: 85vw;

  text-align: left;
  align-contents: center;

  //In order to give equal sizes/fractions to a three column grid, we need to define repeat(3,minmax(0,1fr))
  grid-template-columns: repeat(3, minmax(0, 1fr));
  grid-template-rows: repeat(3, minmax(0, 1fr));
  grid-gap: 2rem;

  @media (max-width: 1350px) {
    width: 95vw;
  }

  @media (max-width: 992px) {
    margin-top: 150px;
    grid-gap: 0.5rem;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-template-areas:
      "div1"
      "div2"
      "div3"
      "div4"
      "div5"
      "div6";
  }
`;
const CEO = styled.div`
  // background-color: teal;

  img {
    width: 200px;
    height: 200px;
    @media (max-width: 1200px) {
      width: 180px;
      height: 180px;
    }
    @media (max-width: 992px) {
      width: 160px;
      height: 160px;
    }
  }
  span {
    text-align: left;

    position: absolute;

    font-family: open-sans;
    font-size: 20px;

    padding-top: 75px;

    @media (max-width: 1200px) {
      font-size: 18px;
      padding-top: 60px;
    }
    @media (max-width: 992px) {
      font-size: 16px;
      padding-top: 50px;
    }
  }

  @media (max-width: 992px) {
    grid-area: div1;
  }
`;
const HeadofIT = styled.div`
  //background-color: teal;
  img {
    width: 200px;
    height: 200px;
    @media (max-width: 1200px) {
      width: 170px;
      height: 170px;
    }

    @media (max-width: 992px) {
      width: 150px;
      height: 150px;
    }
  }
  span {
    position: absolute;
    text-align: left;
    font-family: open-sans;
    font-size: 20px;

    padding-top: 75px;
    @media (max-width: 1200px) {
      font-size: 18px;
      padding-top: 60px;
    }
    @media (max-width: 992px) {
      font-size: 16px;
      padding-top: 50px;
    }
  }

  @media (max-width: 992px) {
    grid-area: div2;
  }
`;

const HeadofAI = styled.div`
  //background-color: teal;
  img {
    width: 200px;
    height: 200px;
    @media (max-width: 1200px) {
      width: 170px;
      height: 170px;
    }

    @media (max-width: 992px) {
      width: 150px;
      height: 150px;
    }
  }
  span {
    position: absolute;
    font-family: open-sans;
    font-size: 20px;

    padding-top: 75px;
    text-align: left;
    @media (max-width: 1200px) {
      font-size: 18px;
      padding-top: 60px;
    }
    @media (max-width: 992px) {
      font-size: 16px;
      padding-top: 50px;
    }
  }

  @media (max-width: 992px) {
    grid-area: div3;
  }
`;

const HeadofSales = styled.div`
  //background-color: teal;
  img {
    width: 200px;
    height: 200px;
    @media (max-width: 1200px) {
      width: 170px;
      height: 170px;
    }

    @media (max-width: 992px) {
      width: 150px;
      height: 150px;
    }
  }
  span {
    position: absolute;
    font-family: open-sans;
    font-size: 20px;

    padding-top: 75px;
    text-align: left;
    @media (max-width: 1200px) {
      font-size: 18px;
      padding-top: 60px;
    }
    @media (max-width: 992px) {
      font-size: 16px;
      padding-top: 50px;
    }
  }

  @media (max-width: 992px) {
    grid-area: div4;
  }
`;
const FullStackdev = styled.div`
  //background-color: teal;
  img {
    width: 200px;
    height: 200px;
    @media (max-width: 1200px) {
      width: 170px;
      height: 170px;
    }

    @media (max-width: 992px) {
      width: 150px;
      height: 150px;
    }
  }
  span {
    position: absolute;
    font-family: open-sans;
    font-size: 20px;

    padding-top: 75px;
    text-align: left;
    @media (max-width: 1200px) {
      font-size: 18px;
      padding-top: 60px;
    }
    @media (max-width: 992px) {
      font-size: 16px;
      padding-top: 50px;
    }
  }

  @media (max-width: 992px) {
    grid-area: div5;
  }
`;
const DataScientist = styled.div`
  //background-color: teal;
  img {
    width: 200px;
    height: 200px;
    @media (max-width: 1200px) {
      width: 170px;
      height: 170px;
    }

    @media (max-width: 992px) {
      width: 150px;
      height: 150px;
    }
  }
  span {
    position: absolute;
    font-family: open-sans;
    font-size: 20px;

    padding-top: 75px;
    text-align: left;
    @media (max-width: 1200px) {
      font-size: 18px;
      padding-top: 60px;
    }
    @media (max-width: 992px) {
      font-size: 16px;
      padding-top: 50px;
    }
  }

  @media (max-width: 992px) {
    grid-area: div6;
  }
`;

const team = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Team</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Team>
        <div></div>
        <CEO>
          <img
            src="/people/christina.svg"
            alt="Christina Hoffman"
            style={{ float: "left" }}
          />
          <span>
            <b>Christina Hoffmann</b>
            <br />
            Founder & CEO
            <br />
            <Link
              href="https://www.linkedin.com/in/christina-hoffmann-9b144311/"
              passHref
            >
              <u style={{ cursor: "pointer" }}>+ more</u>
            </Link>
          </span>
        </CEO>
        <div></div>
        <HeadofIT>
          <img
            src="/people/michel.svg"
            alt="Michel Bevilacqua"
            style={{ float: "left" }}
          />
          <span>
            <b>Michel Bevilacqua</b>
            <br />
            Head of IT
            <br />
            <Link href="https://www.linkedin.com/in/b87/" passHref>
              <u style={{ cursor: "pointer" }}>+ more</u>
            </Link>
          </span>
        </HeadofIT>
        <HeadofAI>
          <img
            src="/people/shivani.svg"
            alt="Shivani Shimpi"
            style={{ float: "left" }}
          />

          <span>
            <b>Shivani Shimpi</b>
            <br />
            Head of AI
            <br />
            <Link
              href="https://www.linkedin.com/in/shivani-shimpi-5113a8170/"
              passHref
            >
              <u style={{ cursor: "pointer" }}>+ more</u>
            </Link>
          </span>
        </HeadofAI>
        <HeadofSales>
          <img
            src="/people/mike.svg"
            alt="Mike Lawrence"
            style={{ float: "left" }}
          />

          <span>
            <b>Mike Lawrence</b>
            <br />
            Head of Sales
            <br />
            <Link
              href="https://www.linkedin.com/in/michael-lawrence-52863014/"
              passHref
            >
              <u style={{ cursor: "pointer" }}>+ more</u>
            </Link>
          </span>
        </HeadofSales>

        <FullStackdev>
          <img
            src="/people/deepika.svg"
            alt="Deepika Mohan"
            style={{ float: "left" }}
          />

          <span>
            <b>Deepika Mohan</b>
            <br />
            Full Stack Developer
            <br />
            <Link href="https://www.linkedin.com/in/deepikachowdhary/" passHref>
              <u style={{ cursor: "pointer" }}>+ more</u>
            </Link>
          </span>
        </FullStackdev>
        <DataScientist>
          <img
            src="/people/timo.svg"
            alt="Timo Streule"
            style={{ float: "left" }}
          />

          <span>
            <b>Timo Streule</b>
            <br />
            Data Scientist
            <br />
            <Link href="https://www.linkedin.com/in/tstreule/" passHref>
              <u style={{ cursor: "pointer" }}>+ more</u>
            </Link>
          </span>
        </DataScientist>
        <div></div>
      </Team>
    </Container>
  );
};

export default team;
