import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  min-height: 200vh;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 1240px) {
    min-height: 275vh;
  }

  @media (max-width: 400px) {
    min-height: 325vh;
  }
`;

const Ul = styled.ul`
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  z-index: 0;
  li {
    span {
      width: 100%;
      height: 100%;
      min-height: 200vh;
      position: absolute;
      top: 0px;
      bottom: 0px;
      left: 0px;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
      z-index: 0;
      animation: imageAnimation 40s linear infinite 0s;
      opacity: 0;

      @media (max-width: 1240px) {
        min-height: 275vh;
      }

      @media (max-width: 400px) {
        min-height: 325vh;
      }
    }

    &:nth-child(1) span {
      background-image: url("/backgrounds/psychometricbg1.svg");
    }
    &:nth-child(2) span {
      background-image: url("/backgrounds/psychometricbg5.svg");
      animation-delay: 5s;
    }
    &:nth-child(3) span {
      background-image: url("/backgrounds/psychometricbg2.svg");
      animation-delay: 10s;
    }
    &:nth-child(4) span {
      background-image: url("/backgrounds/psychometricbg6.svg");
      animation-delay: 15s;
    }

    &:nth-child(5) span {
      background-image: url("/backgrounds/psychometricbg3.svg");
      animation-delay: 20s;
    }
    &:nth-child(6) span {
      background-image: url("/backgrounds/psychometricbg7.svg");
      animation-delay: 25s;
    }
    &:nth-child(7) span {
      background-image: url("/backgrounds/psychometricbg4.svg");
      animation-delay: 30s;
    }
    &:nth-child(8) span {
      background-image: url("/backgrounds/psychometricbg8.svg");
      animation-delay: 35s;
    }

    @keyframes imageAnimation {
      0% {
        opacity: 0;
        animation-timing-function: ease-in;
      }
      8% {
        opacity: 1;
        animation-timing-function: ease-out;
      }
      17% {
        opacity: 1;
      }
      25% {
        opacity: 0;
      }
      100% {
        opacity: 0;
      }
    }
  }
`;

const Content = styled.div`
  margin-top: 20vh;
  z-index: 1000;

  display: grid;
  height: 100vh;
  width: 85vw;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 0.3fr 0.3fr 0.2fr 3.2fr;
  grid-template-rows: 0.5fr 1fr 2.5fr;
  grid-template-areas:
    "div1 div2 div3 div3"
    "div1 div2 div5 div6"
    "div1 div2 div4 div4";

  @media (max-width: 769px) {
    grid-row-gap: 2rem;
    grid-column-gap: 0.5rem;
    width: 95vw;
    grid-template-columns: 0.1fr 0.1fr 0.4fr 3.4fr;
    grid-template-rows: 0.5fr 1fr 2.5fr;
    grid-template-areas:
      "div1 div2 div3 div3"
      "div5 div6 div6 div6"
      "div4 div4 div4 div4";
  }
`;

const Icon1 = styled.div`
  grid-area: div1;

  img {
    @media (max-width: 540px) {
      width: 50px;
      height: 50px;
    }
  }
`;

const Icon2 = styled.div`
  grid-area: div2;
  img{
    @media (max-width: 540px) {
      width:50px;
      height:50px;
    }
`;

const Heading = styled.div`
  grid-area: div3;
  color: rgb(226, 220, 220);
  padding-top: 10px;
  font-weight: bold;
  font-size: 40px;
  font-family: open-sans;

  @media (max-width: 870px) {
    font-size: 35px;
  }

  @media (max-width: 540px) {
    font-size: 25px;
    padding-top: 1px;
  }
  @media (max-width: 290px) {
    font-size: 21px;
    padding-top: 1px;
  }
`;
const SubIcon = styled.div`
  grid-area: div5;
`;
const Subheading = styled.div`
  grid-area: div6;
  font-size: 27px;
  color: white;
  font-family: open-sans;
  word-spacing: 10px;
  padding-top: 28px;

  @media (max-width: 870px) {
    font-size: 23px;
  }

  @media (max-width: 540px) {
    font-size: 21px;
    padding-top: 25px;
    word-spacing: 0px;
  }
`;

const Description = styled.div`
  grid-area: div4;
  font-size: 25px;
  color: white;
  font-family: open-sans;
  text-align: left;
  line-height: 40px;

  @media (max-width: 870px) {
    font-size: 22px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
    padding-top: 1px;
    line-height: 30px;
  }
`;

const psychometric = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Consulting</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Ul>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
      </Ul>
      <Content>
        <Icon1>
          <Link href="/consultingbrain" passHref>
            <img
              src="/icons/backicon.svg"
              alt="Back"
              style={{ cursor: "pointer" }}
            />
          </Link>
        </Icon1>
        <Icon2>
          <img src="/icons/psychometricicon.svg" alt="Icon" />
        </Icon2>
        <Heading>Survey of Psychometric Profiles</Heading>
        <SubIcon>
          <img src="/icons/95percent.svg" alt="95%" />
        </SubIcon>
        <Subheading>
          Customers <b>Say</b>{" "}
          <svg
            width="22"
            height="22"
            viewBox="0 0 22 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            style={{ paddingTop: "10px" }}
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M18.1696 0.338391C17.2518 -0.276201 16.0096 -0.0304054 15.395 0.887391L13.2803 4.0454H2.34901C1.05169 4.0454 0 5.09709 0 6.39441C0 7.69173 1.05169 8.74342 2.34901 8.74342H10.1343L7.72095 12.3474H2.34901C1.05169 12.3474 0 13.3991 0 14.6964C0 15.9937 1.05169 17.0454 2.34901 17.0454H4.57497L3.7739 18.2417C3.1593 19.1595 3.4051 20.4017 4.3229 21.0163C5.24069 21.6309 6.48294 21.3851 7.09753 20.4673L9.38898 17.0454H19.651C20.9483 17.0454 22 15.9937 22 14.6964C22 13.3991 20.9483 12.3474 19.651 12.3474H12.535L14.9483 8.74342H19.651C20.9483 8.74342 22 7.69173 22 6.39441C22 5.09709 20.9483 4.0454 19.651 4.0454H18.0943L18.7186 3.11303C19.3332 2.19523 19.0874 0.952983 18.1696 0.338391Z"
              fill="white"
            />
          </svg>{" "}
          <b>Do</b>
        </Subheading>
        <Description>
          <p>
            Classic market research has its limitations, because people act
            differently than they communicate. Often they act irrationally. In
            addition, previous segmentation according to socio-demographic and /
            or behavioral characteristics in operational marketing fall a little
            short. In direct marketing a persona definition hardly has passed
            the endurance test.
          </p>
          <br />
          <p>
            Socio-demographic characteristics define too broad or general
            segments and behavior-based data is past-driven data. People are
            different and they communicate individually.
          </p>
          <br />
          <p>
            A very valuable addition to the existing segmentations are
            psychometric profiles.
          </p>
          <br />
          <p>
            The personality and its motives provide information about which kind
            of communication (sensory = design, language, visual language,
            haptics, music etc.) is most effective and which kind of product and
            product design should be offered.
          </p>
          <br />
          <p>
            Based on continuous empirical research in Switzerland (Innosuisse)
            and access to decades of research, we are able to help companies
            refine their segmentation and develop practical guidelines. Due to
            the research activities BrandMind was able to develop a validated
            implicit (indirect) playful psychological test with which anonymous
            psychometric profiles can be collected at all digital touchpoints.
            The findings can of course be matched to existing segmentations.
          </p>
        </Description>
      </Content>
    </Container>
  );
};

export default psychometric;
