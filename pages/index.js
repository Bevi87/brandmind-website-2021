import Head from "next/head";
import styled from "styled-components";
import Link from "next/link";

const Container = styled.div`
  min-height: 130vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
const Ul = styled.ul`
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  z-index: 0;
  li {
    span {
      width: 100%;
      height: 100%;
      min-height: 130vh;
      position: absolute;
      top: 0px;
      bottom: 0px;
      left: 0px;
      background-size: cover;
      background-repeat: no-repeat;
      z-index: 0;
      animation: imageAnimation 35s linear infinite 0s;
      opacity: 0;
    }

    &:nth-child(1) span {
      background-image: url("/backgrounds/homebg3.svg");
    }
    &:nth-child(2) span {
      background-image: url("/backgrounds/homebg1.svg");
      animation-delay: 7s;
    }
    &:nth-child(3) span {
      background-image: url("/backgrounds/homebg2.svg");
      animation-delay: 14s;
    }
    &:nth-child(4) span {
      background-image: url("/backgrounds/homebg4.svg");
      animation-delay: 21s;
    }
    &:nth-child(5) span {
      background-image: url("/backgrounds/homebg5.svg");
      animation-delay: 28s;
    }

    @keyframes imageAnimation {
      0% {
        opacity: 0;
        animation-timing-function: ease-in;
      }
      8% {
        opacity: 1;
        animation-timing-function: ease-out;
      }
      17% {
        opacity: 1;
      }
      25% {
        opacity: 0;
      }
      100% {
        opacity: 0;
      }
    }
  }
`;

const Circle = styled.div`
  z-index: 1000;
  background-image: linear-gradient(
    rgb(10, 122, 225, 0.3),
    rgb(255, 13, 13, 0.6)
  );
  position: absolute;
  color: white;
  margin-top: 5vh;

  height: 575px;
  width: 575px;
  text-align: center;
  display: table;
  border-radius: 50%;
  z-index: 1;
  animation: titleAnimation 35s linear infinite 0s;

  @media (max-width: 1024px) {
    position: relative;
    margin-top: 8vh;

    height: 595px;
    width: 595px;
  }

  @media (max-width: 769px) {
    position: relative;
    margin-top: 5vh;

    height: 480px;
    width: 480px;
  }

  @media (max-width: 480px) {
    position: relative;
    margin-top: 7vh;

    height: 345px;
    width: 345px;
  }
  @media (max-width: 360px) {
    position: relative;
    margin-top: 2vh;

    height: 255px;
    width: 255px;
  }
`;

const Heading = styled.h1`
  z-index: 1000;
  color: white;
  font-size: 42px;
  font-style: italic;
  font-family: open-sans;
  font-weight: bold;
  display: table-cell;
  vertical-align: bottom;
  padding-bottom: 23px;
  padding-top: 40px;

  @media (max-width: 1024px) {
    font-size: 36px;
  }

  @media (max-width: 768px) {
    font-size: 35px;
  }

  @media (max-width: 480px) {
    font-size: 28px;
    padding-bottom: 15px;
  }
  @media (max-width: 360px) {
    font-size: 20px;
    padding-bottom: 10px;
  }
`;
const Content = styled.p`
  z-index: 1000;
  color: white;
  font-size: 27px;
  font-family: open-sans;
  font-weight: 500;
  line-height: 50px;
  display: table-row;
  vertical-align: bottom;

  @media (max-width: 1024px) {
    font-size: 27px;
  }

  @media (max-width: 768px) {
    font-size: 24px;
  }

  @media (max-width: 480px) {
    font-size: 21px;
    line-height: 35px;
  }
  @media (max-width: 360px) {
    font-size: 16px;
    line-height: 25px;
  }
`;

const Footer1 = styled.div`
  z-index: 1000;
  display: table-row;
  vertical-align: text-bottom;
  cursor: pointer;

  img {
    @media (max-width: 500px) {
      width: 45px;
      height: 45px;
    }
  }
`;

export default function Home() {
  return (
    <Container>
      <Head>
        <title>Brandmind</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Ul>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
        <li>
          <span />
        </li>
      </Ul>

      <Circle>
        <Heading>We are Brandmind</Heading>
        <Content>
          Swiss startup specialized in neuromarketing.
          <br />
          We provide B2B digital services and consulting.
        </Content>
        <Footer1>
          <Link href="/consulting" passHref>
            <img src="/icons/learnmore.svg" alt="Click here to Learn more" />
          </Link>
        </Footer1>
      </Circle>
    </Container>
  );
}
