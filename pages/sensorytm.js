import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  min-height: 220vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url("/backgrounds/sensorytm.svg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;

  @media (max-width: 1240px) {
    min-height: 300vh;
  }

  @media (max-width: 400px) {
    min-height: 375vh;
  }
`;

const Content = styled.div`
  margin-top: 30vh;
  display: grid;
  height: 100vh;
  width: 85vw;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 0.3fr 0.3fr 3.4fr;
  grid-template-rows: 0.5fr 3.5fr;
  grid-template-areas:
    "div1 div2 div3"
    "div1 div2 div4";

  @media (max-width: 769px) {
    grid-row-gap: 2rem;
    width: 95vw;
    grid-template-column: 0.3fr 0.3fr 3.4fr;
    grid-template-row: 0.5fr 3.5fr;
    grid-template-areas:
      "div1 div2 div3"
      "div4 div4 div4";
  }

  @media (max-width: 469px) {
    grid-row-gap: 1rem;
    margin-top: 25vh;
  }
`;

const Icon1 = styled.div`
  grid-area: div1;

  img {
    @media (max-width: 540px) {
      width: 50px;
      height: 50px;
    }
  }
`;

const Icon2 = styled.div`
  grid-area: div2;
  img{
    @media (max-width: 540px) {
      width:50px;
      height:50px;
    }
`;

const Heading = styled.div`
  grid-area: div3;
  color: rgb(226, 220, 220);
  padding-top: 10px;
  font-weight: bold;
  font-size: 40px;
  font-family: open-sans;
  text-align: left;

  @media (max-width: 870px) {
    font-size: 35px;
  }

  @media (max-width: 540px) {
    font-size: 25px;
    padding-top: 1px;
  }
`;

const Description = styled.div`
  grid-area: div4;
  font-size: 25px;
  color: white;
  font-family: open-sans;
  text-align: left;
  line-height: 40px;

  @media (max-width: 870px) {
    font-size: 22px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
    padding-top: 1px;
    line-height: 30px;
  }
`;

const sensorytm = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Consulting</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Content>
        <Icon1>
          <Link href="/consultingbrain" passHref>
            <img
              src="/icons/backicon.svg"
              alt="Back"
              style={{ cursor: "pointer" }}
            />
          </Link>
        </Icon1>
        <Icon2>
          <img src="/icons/sensorytmicon.svg" alt="Icon" />
        </Icon2>
        <Heading>Sensory Touchpoint Management</Heading>
        <Description>
          <p>
            The biggest challenge in practice is the correct implementation of
            the emotional brand positioning and the matching to the motives of
            the main psychometric target group. The correct coding in
            communication, products, point of sale and sales is often
            implemented to the best of our knowledge and belief.
          </p>
          <br />
          <p>
            However, this step is essential, so that consumers perceive the same
            unconscious codes at every customer contact point and thus have the
            same customer experience everywhere and the brand message really
            reaches the consumer unconsciously. The following codes are relevant
            in the various disciplines of design:
          </p>
          <br />
          <p>
            <h4>Marketing</h4> Colors, shapes, surfaces, haptics, music,
            storytelling, protagonists, lighting conditions for video and
            content ads, newsletters, direct mailings, flyers and giveaways
          </p>
          <br />
          <p>
            <h4>Point of Sale (POS)</h4> Interior design, colors, shapes, music,
            scents, lighting conditions, personnel. Based on the unconscious,
            multisensory perception of human beings, information is conveyed
            within milliseconds before one has dealt with the matter in depth.
            For example, the interior design in combination with the colors and
            shapes can indicate the quality of the service staff or the haptics
            and shape of the packaging of a beverage can indicate the quality of
            the taste.
          </p>
          <br />
          <p>
            <h4>Sales</h4> The direct sales contact is the one with the highest
            impact on customer experience. Both customer service and personal
            sales can be trained on the psychometric profiles. How can the
            salesperson recognize within a few minutes what kind of personality
            is on the phone or in a personal conversation? BrandMind trains
            salespeople to pay attention to posture, tonality, facial
            expressions, language and other behaviors as well as which
            personality types has which character traits and which kind of
            communication connects best with them.
          </p>
        </Description>
      </Content>
    </Container>
  );
};

export default sensorytm;
