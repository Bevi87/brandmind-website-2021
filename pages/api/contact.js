export default function (req, res) {
  require("dotenv").config();

  let nodemailer = require("nodemailer");
  const transporter = nodemailer.createTransport({
    port: 465,
    host: "brandmind.ch",
    auth: {
      user: "info@brandmind.ch",
      pass: "6#2g(2:84ltb",
    },
    secure: true,
  });

  const mailData = {
    from: "info@brandmind.ch",
    to: "info@brandmind.ch",
    subject: `New mail from ${req.body.name}`,
    text: req.body.message + " | Sent from: " + req.body.email,
    html: `<div>${req.body.message}</div><p>Sent from:
    ${req.body.email}</p><p>${req.body.phone}</p><p>${req.body.company}</p>`,
  };
  transporter.sendMail(mailData, function (err, info) {
    if (err) {
      console.log(err);
      res.send("error" + JSON.stringify(err));
    } else {
      console.log(info);
      res.send("success");
    }
  });
}
