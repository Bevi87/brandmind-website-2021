import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  min-height: 220vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url("/backgrounds/neuronales.png");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;

  @media (max-width: 1240px) {
    min-height: 300vh;
  }

  @media (max-width: 870px) {
    min-height: 400vh;
  }
`;

const Content = styled.div`
  margin-top: 30vh;
  display: grid;
  height: 100vh;
  width: 85vw;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 0.3fr 0.3fr 3.4fr;
  grid-template-rows: 0.5fr 3.5fr;
  grid-template-areas:
    "div1 div2 div3"
    "div1 div2 div4";

  @media (max-width: 769px) {
    grid-row-gap: 2rem;
    width: 95vw;
    grid-template-column: 0.3fr 0.3fr 3.4fr;
    grid-template-row: 0.5fr 3.5fr;
    grid-template-areas:
      "div1 div2 div3"
      "div4 div4 div4";
  }
`;

const Icon1 = styled.div`
  grid-area: div1;

  img {
    @media (max-width: 540px) {
      width: 50px;
      height: 50px;
    }
  }
`;

const Icon2 = styled.div`
  grid-area: div2;
  img{
    @media (max-width: 540px) {
      width:50px;
      height:50px;
    }
`;

const Heading = styled.div`
  grid-area: div3;
  color: rgb(226, 220, 220);
  padding-top: 10px;
  font-weight: bold;
  font-size: 40px;
  font-family: open-sans;
  text-align: left;

  @media (max-width: 870px) {
    font-size: 35px;
  }

  @media (max-width: 540px) {
    font-size: 25px;
    padding-top: 1px;
  }
`;

const Description = styled.div`
  grid-area: div4;
  font-size: 25px;
  color: white;
  font-family: open-sans;
  text-align: left;
  line-height: 40px;

  @media (max-width: 870px) {
    font-size: 22px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
    padding-top: 1px;
    line-height: 30px;
  }
`;

const neuronales = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Consulting</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Content>
        <Icon1>
          <Link href="/consultingbrain" passHref>
            <img
              src="/icons/backicon.svg"
              alt="Back"
              style={{ cursor: "pointer" }}
            />
          </Link>
        </Icon1>
        <Icon2>
          <img src="/icons/neuronalesicon.svg" alt="Icon" />
        </Icon2>
        <Heading>Neuronal Empirical Survey</Heading>
        <Description>
          <p>
            In practice, brand positioning and communication is often all about
            the product or the USP. However, this type of brand communicatiion
            does not reach the target audience because it does not trigger
            emotions and it is not inspiring enough. The limbic system in the
            brain -reponsible for the unconscious perception, evaluation and
            triggering of behavior - is not addressed in this way.
          </p>
          <br />
          <p>
            95% of all decisions are made unconsciously -even purchase
            decisions. Various consumer behavior and brain research studies have
            already shown that the emotional enhancement of a brand is not
            achieved through rational derivation. Because human behavior is
            controlled by emotions -by communication that stimulates the limbic
            system: People don't buy what companies do, but WHY companies exist.
            Studies have even shown that clear emotional positioning has an
            effect in people similar to that of religion.
          </p>
          <br />
          <p>
            The basic motives and the authentic culture is the true corporate
            identity. This where companies differ from each other -in people who
            share a belief and values in a company. We support companies in
            sharpening their emotional brand positioning and adopting a clear
            positioning that unconsciously addresses the relevant target group
            (psychometric profiles) in their limbic system.
          </p>
          <br />
          <p>
            The emotional WHY positioning should then also be adequately
            implemented in the brand communciation. In practice, this gap of the
            correct sensory implementation of the emotional brand positioning
            can often be observed. People "tick" differently and perceive
            communciation differently through their senses. Language, images,
            stories, music etc. are all codes in sensor technology and people
            decode within milliseconds in the brain based on their motives,
            experiences and attitudes.
          </p>
          <br />
          <p>
            We support companies and their advertising agencies in finding out
            their emotional WHY and then implement it adequately in the sensory
            codes of communication according to the target group.
          </p>
        </Description>
      </Content>
    </Container>
  );
};

export default neuronales;
