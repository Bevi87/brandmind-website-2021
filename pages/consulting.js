import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  min-height: 160vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url("/backgrounds/consultingbg1.svg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;
`;

const Content = styled.div`
  margin-top: 30vh;
  display: grid;
  height: 80vh;
  width: 50vw;

  grid-row-gap: 2rem;
  grid-template-columns: 0.3fr 0.5fr 3.2fr;
  grid-template-rows: 0.7fr 1fr 1fr 0.7fr 0.6fr;
  grid-template-areas:
    "div1 div2 div2"
    "div1 icon95 div3"
    "div1 icon90 div4"
    "div1 div5 div5"
    "div1 div6 div6";

  @media (max-width: 992px) {
    width: 90vw;
    grid-gap: 2rem;
  }

  @media (max-width: 500px) {
    width: 95vw;
    grid-gap: 1.5rem;
  }
  @media (max-width: 300px) {
    width: 100vw;
  }
`;

const Icon = styled.div`
  grid-area: div1;
  text-align: center;
  img {
    width: 60px;
    height: 60px;

    @media (max-width: 768px) {
      width: 45px;
      height: 45px;
    }
    @media (max-width: 540px) {
      width: 35px;
      height: 35px;
    }
  }
`;

const Heading1 = styled.p`
  grid-area: div2;
  text-align: center;
  font-size: 35px;
  font-family: Open-sans;
  font-weight: bold;

  @media (max-width: 768px) {
    font-size: 30px;
  }
  @media (max-width: 540px) {
    font-size: 22px;
  }
`;

const Icon95 = styled.div`
  grid-area: icon95;
  text-align: right;
  margin-right: 20px;

  @media (max-width: 300px) {
    margin-right: 0px;
  }
`;
const Text1 = styled.p`
  grid-area: div3;
  text-align: left;
  font-size: 30px;
  font-family: open-sans;

  @media (max-width: 768px) {
    font-size: 27px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
  }
`;

const Icon90 = styled.div`
  grid-area: icon90;
  text-align: right;
  margin-right: 20px;

  @media (max-width: 300px) {
    margin-right: 0px;
  }
`;

const Text2 = styled.p`
  grid-area: div4;
  text-align: left;
  font-size: 30px;
  font-family: open-sans;

  @media (max-width: 768px) {
    font-size: 27px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
  }
`;

const Heading2 = styled.p`
  grid-area: div5;
  text-align: center;
  font-size: 35px;
  font-family: Open-sans;
  font-weight: bold;

  @media (max-width: 768px) {
    font-size: 30px;
  }

  @media (max-width: 540px) {
    font-size: 22px;
  }
`;

const Footer1 = styled.div`
  grid-area: div6;
  text-align: center;
  cursor: pointer;
`;

const consulting = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Consulting</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Content>
        <Icon>
          <img src="/icons/consultingicon2.svg" alt="Consulting" />
        </Icon>
        <Heading1>
          We believe in emotions and their impact on buying behavior.
        </Heading1>
        <Icon95>
          <img src="/icons/95percent.svg" alt="95%" />
        </Icon95>

        <Text1>
          of all purchase decisions are made unconsciously and on an emotional
          basis
        </Text1>
        <Icon90>
          <img src="/icons/90percent.svg" alt="90%" />
        </Icon90>
        <Text2>
          of all products and their marketing activities are unemotional and
          exchangeable
        </Text2>
        <Heading2>Why should customers buy and stay loyal?</Heading2>
        <Footer1>
          <Link href="/consultingbrain" passHref>
            <img src="/icons/learnmore.svg" alt="Click here to Learn more" />
          </Link>
        </Footer1>
      </Content>
    </Container>
  );
};

export default consulting;
