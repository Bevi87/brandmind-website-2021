import Head from "next/head";
import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  min-height: 200vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url("/backgrounds/deepdive.svg");
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  color: white;
  position: relative;

  @media (max-width: 1240px) {
    min-height: 275vh;
  }

  @media (max-width: 400px) {
    min-height: 350vh;
  }
`;

const Content = styled.div`
  margin-top: 30vh;
  display: grid;
  height: 100vh;
  width: 85vw;
  grid-column-gap: 1rem;
  grid-row-gap: 1rem;
  grid-template-columns: 0.3fr 0.3fr 3.4fr;
  grid-template-rows: 0.5fr 3.5fr;
  grid-template-areas:
    "div1 div2 div3"
    "div1 div2 div4";

  @media (max-width: 769px) {
    grid-row-gap: 2rem;
    grid-column-gap: 0.5rem;
    width: 95vw;
    grid-template-column: 0.3fr 0.3fr 3.4fr;
    grid-template-row: 0.5fr 3.5fr;
    grid-template-areas:
      "div1 div2 div3"
      "div4 div4 div4";
  }
`;

const Icon1 = styled.div`
  grid-area: div1;

  img {
    @media (max-width: 540px) {
      width: 50px;
      height: 50px;
    }
  }
`;

const Icon2 = styled.div`
  grid-area: div2;
  img{
    @media (max-width: 540px) {
      width:50px;
      height:50px;
    }
`;

const Heading = styled.div`
  grid-area: div3;
  color: rgb(226, 220, 220);
  padding-top: 10px;
  font-weight: bold;
  font-size: 40px;
  font-family: open-sans;
  text-align: left;

  @media (max-width: 870px) {
    font-size: 35px;
  }

  @media (max-width: 540px) {
    font-size: 25px;
    padding-top: 1px;
  }
`;

const Description = styled.div`
  grid-area: div4;
  font-size: 25px;
  color: white;
  font-family: open-sans;
  text-align: left;
  line-height: 40px;

  @media (max-width: 870px) {
    font-size: 22px;
  }

  @media (max-width: 540px) {
    font-size: 20px;
    padding-top: 1px;
    line-height: 30px;
  }
`;

const deepdive = () => {
  return (
    <Container>
      <Head>
        <title>Brandmind - Consulting</title>
        <link rel="icon" href="/bmfavicon.ico" />
      </Head>
      <Content>
        <Icon1>
          <Link href="/consultingbrain" passHref>
            <img
              src="/icons/backicon.svg"
              alt="Back"
              style={{ cursor: "pointer" }}
            />
          </Link>
        </Icon1>
        <Icon2>
          <img src="/icons/deepdiveicon.svg" alt="Icon" />
        </Icon2>
        <Heading>Deep Dive Psychological Interviews</Heading>
        <Description>
          <p>
            Deep dive psychological interviews are one-to two-hour projective,
            qualitative interview procedures. The aim of these in-depth
            interviews is to establish a connection between the motives and
            individual features / characteristics of products. Product-specific
            psychometric profiles can be developed from this interview
            technique.
          </p>
          <br />
          <p>
            BrandMind combines these interviews with neuronal measuring
            instruments (EEG, pulse measurement, micro mimics, fMRI). The
            emotional state of excitement is measured using neuronal measurement
            methods. This makes it possible to determine whether the spoken word
            has the same meaning as the emotional state of excitement. BrandMind
            thus combines neuromarketing with psychology.
          </p>
          <br />
          <p>
            Deep dive psychological interviews make sense especially under the
            following conditions:
          </p>
          <br />
          <p>
            <h3>Innovative product development</h3>
            <li>
              For the new{" "}
              <b>
                <i>development / further development</i>
              </b>{" "}
              of{" "}
              <b>
                <i>products / services</i>
              </b>
            </li>
            <li>
              For the{" "}
              <b>
                <i>reduction of complex, high-quality products</i>
              </b>{" "}
              or in the case of products that already{" "}
              <b>
                <i>evoke too strong an association among consumers</i>
              </b>{" "}
              (e.g. such as Orange juice).
            </li>
          </p>
          <br />
          <p>
            <h3>In-depth market research</h3>
            The psychological in-depth interviews are excellent for gaining a
            deeper understanding of which motives motivate consumers to prefer
            or even buy products.
            <br /> In deep dive psychological interviews, aspects emerge that
            cannot emerge in a classical, quantitative survey.
          </p>
        </Description>
      </Content>
    </Container>
  );
};

export default deepdive;
