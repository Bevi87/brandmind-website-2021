import Footer from "./Footer";
import Navigationbar from "./Navigationbar";

const Layout = ({ children }) => {
  return (
    <div>
      <Navigationbar />
      {children}
      <Footer />
    </div>
  );
};

export default Layout;
