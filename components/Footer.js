import styled from "styled-components";
import Link from "next/link";

const FooterSection = styled.div`
  background: black;
  color: white;
  display: flex;
  height: 150px;

  width: 95vw;
  bottom: 0;
  z-index: 1000;
  justify-content: center;
  align-items: center;
  position: relative;
`;
const Ul = styled.ul`
  list-style: none;
  text-align: center;
  line-height: 40px;
`;
const Heading = styled.li`
  font-size: 24px;
  font-family: Open-sans;

  @media (max-width: 768px) {
    font-size: 18px;
  }

  @media (max-width: 500px) {
    font-size: 12px;
  }
`;
const Facebook = styled.li`
  font-size: 22px;
  font-family: Open-sans;
  display: inline;
  padding-right: 30px;
  @media (max-width: 768px) {
    font-size: 14px;
  }
  @media (max-width: 500px) {
    font-size: 10px;
  }
`;
const LinkedIn = styled.li`
  font-size: 22px;
  font-family: Open-sans;
  display: inline;
  @media (max-width: 768px) {
    font-size: 14px;
  }
  @media (max-width: 500px) {
    font-size: 10px;
  }
`;

const Footer = () => {
  return (
    <FooterSection>
      <Ul>
        <Heading>Copyright &#169; 2021 Brandmind</Heading>

        <Facebook>
          <Link
            href="https://www.linkedin.com/company/brandmind-agency/"
            passHref
          >
            Facebook
          </Link>
        </Facebook>

        <LinkedIn>
          <Link
            href="https://www.linkedin.com/company/brandmind-agency/"
            passHref
          >
            LinkedIn
          </Link>
        </LinkedIn>
      </Ul>
    </FooterSection>
  );
};

export default Footer;
