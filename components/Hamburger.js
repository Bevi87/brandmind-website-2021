import React, { useState } from "react";
import styled from "styled-components";
import Rightnav from "./Rightnav";

const Styledburger = styled.div`
  width: 2rem;
  height: 2rem;
  position: relative;
  top: 25px;
  right: 20px;
  display: flex;

  z-index: 1;
  display: none;

  @media (max-width: 1030px) {
    display: flex;
    justify-content: space-around;
    flex-flow: column nowrap;
  }

  div {
    width: 2rem;
    height: 0.15rem;
    background-color: ${({ open }) => (open ? "grey" : "white")};
    transform-origin: 1px;
    transition: all 0.3s linear;

    &:nth-child(1) {
      transform: ${({ open }) => (open ? "rotate(45deg)" : "rotate(0)")};
    }

    &:nth-child(2) {
      transform: ${({ open }) => (open ? "translateX(100%)" : "translateX(0)")};
      opacity: ${({ open }) => (open ? 0 : 1)};
    }

    &:nth-child(3) {
      transform: ${({ open }) => (open ? "rotate(-45deg)" : "rotate(0)")};
    }
  }
`;

const Hamburger = () => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Styledburger open={open} onClick={() => setOpen(!open)}>
        <div />
        <div />
        <div />
      </Styledburger>
      <Rightnav open={open} />
    </>
  );
};

export default Hamburger;
