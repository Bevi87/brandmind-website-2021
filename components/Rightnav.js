import Link from "next/link";
import { useState } from "react";
import styled from "styled-components";

const Ul = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;

  li {
    padding: 28px 10px;
  }

  @media (max-width: 1030px) {
    flex-flow: column nowrap;
    background-color: black;
    opacity: 0.9;
    position: fixed;
    transform: ${({ open }) => (open ? "translateX(0)" : "translateX(100%)")};
    top: 0px;
    right: 0;
    height: fit-content;
    width: 100%;
    padding-top: 7.5rem;
    text-align: center;
    z-index: 0;
    li {
      color: white;
      z-index: 20;
    }
  }
`;

const StyledLink = styled.a`
  padding: 0rem 3rem;
  font-size: 28px;
  font-weight: light;
  font-family: open-sans;
  font-style: italic;
  z-index: 20;
  color: white;

  &:hover {
    font-weight: 900;
    color: white;

    span {
      visibility: visible;
    }
  }
  &:focus {
    font-weight: 900;
    color: white;
    font-style: normal;

    span {
      visibility: visible;
    }
  }

  @media (max-width: 1030px) {
    font-size: 24px;
    span {
      display: none;
    }
  }

  span {
    visibility: hidden;
    text-align: center;
    z-index: 20;
    top: 120%;
    left: 50%;
  }
`;

const Rightnav = ({ open }) => {
  return (
    <Ul open={open}>
      <li>
        <Link href="/" passHref>
          <StyledLink>
            HOME
            <br />
            <span style={{ paddingLeft: "4rem" }}>
              <svg
                width="40"
                height="40"
                viewBox="0 0 40 40"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g filter="url(#filter0_d)">
                  <rect
                    x="4.5"
                    y="4.5"
                    width="31"
                    height="31"
                    rx="9.5"
                    stroke="white"
                  />
                  <path
                    d="M17.9995 22V27H13.9995V18L19.9995 13L25.9995 18V27H21.9995V22H17.9995Z"
                    stroke="white"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </g>
                <defs>
                  <filter
                    id="filter0_d"
                    x="0"
                    y="0"
                    width="40"
                    height="40"
                    filterUnits="userSpaceOnUse"
                    color-interpolation-filters="sRGB"
                  >
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix
                      in="SourceAlpha"
                      type="matrix"
                      values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix
                      type="matrix"
                      values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0"
                    />
                    <feBlend
                      mode="normal"
                      in2="BackgroundImageFix"
                      result="effect1_dropShadow"
                    />
                    <feBlend
                      mode="normal"
                      in="SourceGraphic"
                      in2="effect1_dropShadow"
                      result="shape"
                    />
                  </filter>
                </defs>
              </svg>
            </span>
          </StyledLink>
        </Link>
      </li>
      <li>
        <Link href="/consulting" passHref>
          <StyledLink>
            CONSULTING
            <br />
            <span style={{ paddingLeft: "7rem" }}>
              <svg
                width="40"
                height="40"
                viewBox="0 0 40 40"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g filter="url(#filter0_d)">
                  <rect
                    x="4.5"
                    y="4.5"
                    width="31"
                    height="31"
                    rx="9.5"
                    stroke="white"
                  />
                  <path
                    d="M25.6666 20.8182C26.0666 20.8182 26.3999 20.9455 26.5999 21.2C26.8666 21.4545 26.9999 21.7727 26.9999 22.0909L21.6666 24L16.9999 22.7273V17H18.2666L23.1333 18.7182C23.4666 18.8455 23.6666 19.1 23.6666 19.4182C23.6666 19.6091 23.5999 19.8 23.4666 19.9273C23.3333 20.0545 23.1333 20.1818 22.8666 20.1818H20.9999L19.8666 19.7364L19.6666 20.3091L20.9999 20.8182H25.6666V20.8182ZM12.9999 17H15.6666V24H12.9999V17Z"
                    stroke="white"
                    stroke-linejoin="round"
                  />
                </g>
                <defs>
                  <filter
                    id="filter0_d"
                    x="0"
                    y="0"
                    width="40"
                    height="40"
                    filterUnits="userSpaceOnUse"
                    color-interpolation-filters="sRGB"
                  >
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix
                      in="SourceAlpha"
                      type="matrix"
                      values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix
                      type="matrix"
                      values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0"
                    />
                    <feBlend
                      mode="normal"
                      in2="BackgroundImageFix"
                      result="effect1_dropShadow"
                    />
                    <feBlend
                      mode="normal"
                      in="SourceGraphic"
                      in2="effect1_dropShadow"
                      result="shape"
                    />
                  </filter>
                </defs>
              </svg>
            </span>
          </StyledLink>
        </Link>
      </li>
      <li>
        <Link href="/team" passHref>
          <StyledLink>
            TEAM
            <br />
            <span style={{ paddingLeft: "3.8rem" }}>
              <svg
                width="41"
                height="40"
                viewBox="0 0 41 40"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g filter="url(#filter0_d)">
                  <rect
                    x="5"
                    y="4.5"
                    width="31"
                    height="31"
                    rx="9.5"
                    stroke="white"
                  />
                  <path
                    d="M20.4752 19.5502C21.8298 19.5502 22.8833 18.4465 22.8833 17.0919C22.8833 15.7373 21.7796 14.6837 20.425 14.6837C19.0704 14.6837 18.0168 15.7875 18.0168 17.0919C18.0168 18.4465 19.1206 19.5502 20.4752 19.5502ZM20.425 15.6871C20.4752 15.6871 20.4752 15.6871 20.425 15.6871C21.2277 15.6871 21.8799 16.3393 21.8799 17.142C21.8799 17.9448 21.2277 18.5468 20.425 18.5468C19.6223 18.5468 19.0202 17.8946 19.0202 17.142C19.0202 16.3393 19.6725 15.6871 20.425 15.6871Z"
                    fill="white"
                  />
                  <path
                    d="M27.9 19.2492C26.9467 18.3963 25.6925 17.9448 24.388 17.995H23.9867C23.8863 18.3963 23.7358 18.7475 23.5352 19.0486C23.8362 18.9984 24.087 18.9984 24.388 18.9984C25.3413 18.9482 26.2945 19.2492 27.0471 19.8011V23.4134H28.0505V19.3997L27.9 19.2492Z"
                    fill="white"
                  />
                  <path
                    d="M23.2344 14.784C23.4852 14.182 24.1876 13.881 24.8398 14.1318C25.4418 14.3827 25.7429 15.085 25.492 15.7373C25.2913 16.1888 24.8398 16.4898 24.3883 16.4898C24.2879 16.4898 24.1374 16.4898 24.0371 16.4396C24.0873 16.6905 24.0873 16.9413 24.0873 17.142V17.443C24.1876 17.443 24.2879 17.4932 24.3883 17.4932C25.6425 17.4932 26.6459 16.4898 26.6459 15.2857C26.6459 14.0315 25.6425 13.0281 24.4384 13.0281C23.6357 13.0281 22.9333 13.4294 22.532 14.1318C22.7828 14.2823 23.0337 14.483 23.2344 14.784Z"
                    fill="white"
                  />
                  <path
                    d="M17.5148 19.0986C17.3141 18.7976 17.1636 18.4464 17.0633 18.045H16.6619C15.3575 17.9949 14.1033 18.4464 13.15 19.2491L12.9995 19.3996V23.4132H14.0029V19.801C14.8056 19.2491 15.7087 18.9481 16.6619 18.9983C16.9629 18.9983 17.264 19.0484 17.5148 19.0986Z"
                    fill="white"
                  />
                  <path
                    d="M16.6621 17.4431C16.7624 17.4431 16.8628 17.4431 16.9631 17.3929V17.0919C16.9631 16.841 16.9631 16.5902 17.0133 16.3895C16.913 16.4397 16.7624 16.4397 16.6621 16.4397C16.0099 16.4397 15.458 15.8878 15.458 15.2356C15.458 14.5834 16.0099 14.0315 16.6621 14.0315C17.1638 14.0315 17.6153 14.3325 17.816 14.7841C18.0167 14.5332 18.3177 14.2824 18.5686 14.0817C17.9164 13.0281 16.5618 12.6769 15.5082 13.3291C14.4546 13.9813 14.1034 15.3359 14.7556 16.3895C15.157 17.0417 15.8594 17.4431 16.6621 17.4431Z"
                    fill="white"
                  />
                  <path
                    d="M24.5886 22.2594L24.4883 22.1089C23.4849 21.0051 22.0801 20.3529 20.575 20.4031C19.0699 20.3529 17.615 21.0051 16.6116 22.1089L16.5112 22.2594V26.0723C16.5112 26.5238 16.8624 26.9252 17.3641 26.9252H23.7859C24.2374 26.9252 24.6388 26.5238 24.6388 26.0723V22.2594H24.5886ZM23.5852 25.9218H17.5146V22.6106C18.3174 21.8079 19.4211 21.4065 20.575 21.4065C21.6787 21.3563 22.7825 21.8079 23.5852 22.6106V25.9218Z"
                    fill="white"
                  />
                </g>
                <defs>
                  <filter
                    id="filter0_d"
                    x="0.5"
                    y="0"
                    width="40"
                    height="40"
                    filterUnits="userSpaceOnUse"
                    color-interpolation-filters="sRGB"
                  >
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix
                      in="SourceAlpha"
                      type="matrix"
                      values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix
                      type="matrix"
                      values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0"
                    />
                    <feBlend
                      mode="normal"
                      in2="BackgroundImageFix"
                      result="effect1_dropShadow"
                    />
                    <feBlend
                      mode="normal"
                      in="SourceGraphic"
                      in2="effect1_dropShadow"
                      result="shape"
                    />
                  </filter>
                </defs>
              </svg>
            </span>
          </StyledLink>
        </Link>
      </li>
      <li>
        <Link href="/contactus" passHref>
          <StyledLink>
            CONTACT
            <br />
            <span style={{ paddingLeft: "5rem" }}>
              <svg
                width="41"
                height="40"
                viewBox="0 0 41 40"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g filter="url(#filter0_d)">
                  <rect
                    x="5"
                    y="4.5"
                    width="31"
                    height="31"
                    rx="9.5"
                    stroke="white"
                  />
                  <path
                    d="M26.9551 17.2875C26.6038 16.4508 26.0927 15.6903 25.4503 15.0484C24.8099 14.4054 24.0491 13.8942 23.2112 13.5438C22.3529 13.183 21.4306 12.9981 20.4993 13H20.468C19.5206 13.0047 18.6046 13.1922 17.7419 13.561C16.9112 13.915 16.1576 14.4271 15.5232 15.0688C14.887 15.7092 14.3818 16.467 14.0356 17.3C13.6763 18.1662 13.4942 19.0954 13.5001 20.0328C13.5048 21.1172 13.7648 22.1938 14.2502 23.1563V25.5313C14.2502 25.9281 14.5727 26.25 14.9689 26.25H17.3457C18.3148 26.7379 19.3841 26.9947 20.4695 27H20.5024C21.4387 27 22.3453 26.8188 23.2003 26.4641C24.0339 26.1178 24.7918 25.6125 25.4315 24.9766C26.0766 24.3375 26.584 23.5906 26.9394 22.7578C27.3074 21.8953 27.4953 20.9781 27.5 20.0313C27.5031 19.0797 27.3183 18.1563 26.9551 17.2875ZM17.3739 20.75C16.9606 20.75 16.6239 20.4141 16.6239 20C16.6239 19.5859 16.9606 19.25 17.3739 19.25C17.7873 19.25 18.1239 19.5859 18.1239 20C18.1239 20.4141 17.7889 20.75 17.3739 20.75ZM20.4993 20.75C20.0859 20.75 19.7492 20.4141 19.7492 20C19.7492 19.5859 20.0859 19.25 20.4993 19.25C20.9126 19.25 21.2493 19.5859 21.2493 20C21.2493 20.4141 20.9126 20.75 20.4993 20.75ZM23.6246 20.75C23.2112 20.75 22.8746 20.4141 22.8746 20C22.8746 19.5859 23.2112 19.25 23.6246 19.25C24.038 19.25 24.3746 19.5859 24.3746 20C24.3746 20.4141 24.038 20.75 23.6246 20.75Z"
                    stroke="white"
                  />
                </g>
                <defs>
                  <filter
                    id="filter0_d"
                    x="0.5"
                    y="0"
                    width="40"
                    height="40"
                    filterUnits="userSpaceOnUse"
                    color-interpolation-filters="sRGB"
                  >
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix
                      in="SourceAlpha"
                      type="matrix"
                      values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                    />
                    <feOffset />
                    <feGaussianBlur stdDeviation="2" />
                    <feColorMatrix
                      type="matrix"
                      values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0"
                    />
                    <feBlend
                      mode="normal"
                      in2="BackgroundImageFix"
                      result="effect1_dropShadow"
                    />
                    <feBlend
                      mode="normal"
                      in="SourceGraphic"
                      in2="effect1_dropShadow"
                      result="shape"
                    />
                  </filter>
                </defs>
              </svg>
            </span>
          </StyledLink>
        </Link>
      </li>
    </Ul>
  );
};

export default Rightnav;
